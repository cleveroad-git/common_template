//
//  RSAEncryptionDecryption.h
//  RSA_template
//
//  Created by Gromov on 09.02.16.
//  Copyright © 2016 cleveroad. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface RSAEncryptionDecryption : NSObject

- (void)setPublicTag:(NSData *)publicTag privateTag:(NSData *)privateTag;
- (void)setTestTags;
- (void)testAsymmetricEncryptionAndDecryption;

- (void)encryptWithPublicKey:(uint8_t *)plainBuffer cipherBuffer:(uint8_t *)cipherBuffer;
- (void)decryptWithPrivateKey:(uint8_t *)cipherBuffer plainBuffer:(uint8_t *)plainBuffer;

- (void)addPeerPublicKey:(NSString *)peerName keyBits:(NSData *)publicKeyData;
- (SecKeyRef)getPublicKeyReferenceByName:(NSString*)peerName;

@end
