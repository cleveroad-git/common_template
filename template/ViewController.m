//
//  ViewController.m
//  template
//
//  Created by Gromov on 02.11.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "ViewController.h"
#import "AxThrobber.h"
#import "Common.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [AxThrobber defaultThrobberOnView:self.view withTitle:@"Title" details:@"details" displayMode:ThrobberModeCustomViewWithoutTitles];
    [[AxThrobber defaultThrobber] show];
}

@end
