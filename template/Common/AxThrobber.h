//
//  AxThrobber.h
//
//  Created by Vyacheslav Mirgorod on 07/15/11
//  Modified by Andrew Kovalev on 09/01/11
//

#import <UIKit/UIKit.h>

@class RoundProgressView;


#define kThrobberSize      CGSizeMake(160.0, 120.0)
#define kThrobberTitleY    25.0
#define kThrobberDetailsY  50.0


typedef enum {
  ThrobberModeActivity = 0,
	ThrobberModeProgress = 1,
  ThrobberModeCustomView = 3,
  ThrobberModeCustomViewWithoutTitles = 4,
} ThrobberMode;


//
// AxThrobber
//

@interface AxThrobber : UIView
{
  UIView                  *contentView;
  UIActivityIndicatorView *activityView;
  RoundProgressView       *progressView;
  UILabel                 *titleLabel, *detailsLabel;
  
  CGFloat                 opacity;
  BOOL                    gradient;
}
@property(nonatomic, retain) NSString *title, *details;
@property(nonatomic, retain) UIView *customView;
@property(nonatomic, assign) ThrobberMode mode;
@property(nonatomic, assign) CGFloat progress;

+ (AxThrobber *)defaultThrobber;
+ (void)defaultThrobberOnView:(UIView *)parentView withTitle:(NSString *)title details:(NSString *)details displayMode:(ThrobberMode)aMode;

- (id)initWithFrame:(CGRect)frame;
- (void)show;
- (void)dismiss;

@end




