//
//  AxThrobber.m
//  
//  Created by Vyacheslav Mirgorod on 07/15/11
//  Modified by Andrew Kovalev on 09/01/11
//

#import "AxThrobber.h"
#import "Common.h"


#if !defined(CGRectGetMidPoint)
#define CGRectGetMidPoint(rect) CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect))
#endif

//
// RoundProgressView
//

@interface RoundProgressView : UIView
@property(nonatomic, assign) float progress;
@end


@implementation RoundProgressView
@synthesize progress;

- (id)init {
  return [self initWithFrame:CGRectMake(0.0f, 0.0f, 37.0f, 37.0f)];
}

- (id)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame])) {
    self.backgroundColor = [UIColor clearColor];
		self.opaque = NO;
  }
  return self;
}

- (float)progress {
  return progress;
}

- (void)setProgress:(float)aProgress {
  progress = aProgress;
  [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
  CGRect allRect = self.bounds;
  CGRect circleRect = CGRectInset(allRect, 2.0f, 2.0f);
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // Draw background
  CGContextSetRGBStrokeColor(context, 1.0f, 1.0f, 1.0f, 1.0f); // white
  CGContextSetRGBFillColor(context, 1.0f, 1.0f, 1.0f, 0.1f); // translucent white
  CGContextSetLineWidth(context, 2.0f);
  CGContextFillEllipseInRect(context, circleRect);
  CGContextStrokeEllipseInRect(context, circleRect);
  
  // Draw progress
  CGPoint center = CGPointMake(allRect.size.width / 2, allRect.size.height / 2);
  CGFloat radius = (allRect.size.width - 4) / 2;
  CGFloat startAngle = -((float)M_PI / 2);  // 90 degrees
  CGFloat endAngle = (progress * 2 * (float)M_PI) + startAngle;
  CGContextSetRGBFillColor(context, 1.0f, 1.0f, 1.0f, 1.0f);  // white
  CGContextMoveToPoint(context, center.x, center.y);
  CGContextAddArc(context, center.x, center.y, radius, startAngle, endAngle, 0);
  CGContextClosePath(context);
  CGContextFillPath(context);
}

@end


//
// AxThrobber
//

static AxThrobber *defaultThrobber = nil;

@implementation AxThrobber
@synthesize title, details, customView, mode, progress;

+ (AxThrobber *)defaultThrobber {
  return defaultThrobber;
}

+ (void)defaultThrobberOnView:(UIView *)parentView
                    withTitle:(NSString *)title
                      details:(NSString *)details
                  displayMode:(ThrobberMode)aMode
{
  if (defaultThrobber.superview != parentView) {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [defaultThrobber removeFromSuperview];
    defaultThrobber = nil;
  }
  
  if (parentView != nil) {
    defaultThrobber = [[AxThrobber alloc] initWithFrame:parentView.bounds];
    defaultThrobber.title = title;
    defaultThrobber.details = details;
    defaultThrobber.mode = aMode;
    defaultThrobber.hidden = YES;
    //defaultThrobber.backgroundColor = [UIColor colorWithRed:1.0 green:0.0 blue:0.0 alpha:0.5];
    defaultThrobber.userInteractionEnabled = YES;
    [parentView addSubview:defaultThrobber];
    
    [[NSNotificationCenter defaultCenter] addObserver:defaultThrobber
                                             selector:@selector(changeOrientationCallback:)
                                                 name:UIApplicationDidChangeStatusBarOrientationNotification
                                               object:nil];
  }
}

- (id)initWithFrame:(CGRect)frame {
  if ((self = [super initWithFrame:frame]))
  {
    gradient = NO;
    opacity = 0.8;
    self.autoresizingMask = UIViewAutoresizingNone;
    self.alpha = 0.0;
    self.backgroundColor = [UIColor clearColor];
    
    contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 
                                                           0, 
                                                           kThrobberSize.width,
                                                           kThrobberSize.height)];
    [self addSubview:contentView];
    contentView.autoresizingMask = UIViewAutoresizingNone;
    contentView.backgroundColor = [UIColor clearColor];
    contentView.center = CGRectGetMidPoint(self.bounds);
    
    activityView = [[UIActivityIndicatorView alloc] 
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityView.hidesWhenStopped = NO;
    [contentView addSubview:activityView];
    activityView.center = CGRectGetMidPoint(contentView.bounds);
    
    progressView = [[RoundProgressView alloc] initWithFrame:activityView.frame];
    [contentView addSubview:progressView];
    progressView.center = CGRectGetMidPoint(contentView.bounds);
    
    titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textAlignment = UITextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    [contentView addSubview:titleLabel];
        
    detailsLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    detailsLabel.backgroundColor = [UIColor clearColor];
    detailsLabel.textAlignment = UITextAlignmentCenter;
    detailsLabel.textColor = [UIColor whiteColor];
    detailsLabel.font = [UIFont boldSystemFontOfSize:13];
    detailsLabel.lineBreakMode = UILineBreakModeWordWrap;
    detailsLabel.numberOfLines = 2;
    [contentView addSubview:detailsLabel];
    
    self.mode = ThrobberModeActivity;
  }
  return self;
}

#pragma mark -

- (void)setCustomView:(UIView *)aCustomView
{
  if (customView != aCustomView) {
    [customView removeFromSuperview];
    customView = aCustomView;
    
    [contentView addSubview:customView];
    customView.center = CGRectGetMidPoint(contentView.bounds);
    
    if (mode == ThrobberModeCustomView)
      [self setMode:mode];
  }
}

- (void)setMode:(ThrobberMode)aMode
{
  mode = aMode;
  
  if (customView && mode >= ThrobberModeCustomView) {
    [activityView stopAnimating];
    activityView.hidden = YES;
    progressView.hidden = YES;
    if (mode > ThrobberModeCustomView) {
      titleLabel.hidden = YES;
      detailsLabel.hidden = YES;
    }
    customView.hidden = NO;
  }
  else if (mode == ThrobberModeActivity) {
    customView.hidden = YES;
    titleLabel.hidden = NO;
    detailsLabel.hidden = NO;
    progressView.hidden = YES;
    activityView.hidden = NO;
    [activityView startAnimating];
  }
  else {
    titleLabel.hidden = NO;
    detailsLabel.hidden = NO;
    progressView.hidden = NO;
    [activityView stopAnimating];
    activityView.hidden = YES;
    customView.hidden = YES;
  }
}

- (void)setProgress:(CGFloat)aProgress
{
  progress = aProgress;
  [progressView setProgress:progress];
}

- (NSString *)title {
  return titleLabel.text;
}

- (void)setTitle:(NSString *)aTitle
{
  titleLabel.text = aTitle;
  [self setNeedsLayout];
}

- (NSString *)details {
  return detailsLabel.text;
}

- (void)setDetails:(NSString *)aDetails
{
  detailsLabel.text = aDetails;
  [self setNeedsLayout];
}

#pragma mark -

- (void)drawRect:(CGRect)rect
{
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  if (gradient) {
    //Gradient colours
    size_t gradLocationsNum = 2;
    CGFloat gradLocations[2] = {0.0f, 1.0f};
    CGFloat gradColors[8] = {0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.0f,0.75f}; 
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient_ = CGGradientCreateWithColorComponents(colorSpace, 
                                                                  gradColors, 
                                                                  gradLocations, 
                                                                  gradLocationsNum);
		CGColorSpaceRelease(colorSpace);
    
    //Gradient center
    CGPoint gradCenter = CGPointMake(self.bounds.size.width/2, 
                                     self.bounds.size.height/2);
    //Gradient radius
    float gradRadius = MIN(self.bounds.size.width , self.bounds.size.height);
    //Gradient draw
    CGContextDrawRadialGradient (context, gradient_, gradCenter,
                                 0, gradCenter, gradRadius,
                                 kCGGradientDrawsAfterEndLocation);
		CGGradientRelease(gradient_);
  }    
  
  // Center Throbber
  //CGRect allRect = self.bounds;
  // Draw rounded Throbber bacgroud rect
  CGRect boxRect = contentView.frame;
	// Corner radius
	float r = 10.0f;
  CGFloat minx = CGRectGetMinX(boxRect), maxx = CGRectGetMaxX(boxRect);
  CGFloat miny = CGRectGetMinY(boxRect), maxy = CGRectGetMaxY(boxRect);
	
  CGContextBeginPath(context);
  CGContextSetGrayFillColor(context, 0.0f, opacity);
  CGContextMoveToPoint(context, minx + r, CGRectGetMinY(boxRect));
  CGContextAddArc(context, maxx - r, miny + r, r, 3 * (float)M_PI / 2, 0, 0);
  CGContextAddArc(context, maxx - r, maxy - r, r, 0, (float)M_PI / 2, 0);
  CGContextAddArc(context, minx + r, maxy - r, r, (float)M_PI / 2, (float)M_PI, 0);
  CGContextAddArc(context, minx + r, miny + r, r, (float)M_PI, 3 * (float)M_PI / 2, 0);
  CGContextClosePath(context);
  CGContextFillPath(context);
}

- (void)layoutSubviews
{
  CGRect rect = self.superview.bounds;
  CGAffineTransform transform = CGAffineTransformMakeRotation(0);
  
  if ([self.superview isKindOfClass:[UIWindow class]]) {
    rect = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    switch ([[UIApplication sharedApplication] statusBarOrientation]) {
      case UIInterfaceOrientationLandscapeLeft:
        transform = CGAffineTransformMakeRotation(-0.5 * M_PI);
        break;
      case UIInterfaceOrientationLandscapeRight:
        transform = CGAffineTransformMakeRotation(0.5 * M_PI);
        break;
      case UIInterfaceOrientationPortrait:
        transform = CGAffineTransformMakeRotation(0);
        break;
      case UIInterfaceOrientationPortraitUpsideDown:
        transform = CGAffineTransformMakeRotation(M_PI);
        break;
      default:
        break;
    }
  }
  
  self.frame = rect;
  contentView.transform = transform;
  
  //
  
  contentView.center = CGRectGetMidPoint(self.bounds);
  
  activityView.center = CGRectGetMidPoint(contentView.bounds);
  progressView.center = CGRectGetMidPoint(contentView.bounds);
  customView.center   = CGRectGetMidPoint(contentView.bounds);
  
  float x = CGRectGetMinX(contentView.bounds);
  float y = CGRectGetMidY(contentView.bounds);
  float w = CGRectGetWidth(contentView.bounds);
  
  CGRect titleRect = CGRectMake(x, y, w, 0);
  if (!titleLabel.hidden && titleLabel.text && ![titleLabel.text isEqualToString:@""]) {
    CGSize titleSize = [titleLabel.text sizeWithFont:titleLabel.font];
    titleRect = CGRectMake(x, y+kThrobberTitleY, w, titleSize.height);
  }
  titleLabel.frame = titleRect;
  
  CGRect detailsRect = CGRectMake(x, y, w, 0);
  if (!detailsLabel.hidden && detailsLabel.text && ![detailsLabel.text isEqualToString:@""]) {
    CGSize detailsSize = [detailsLabel.text sizeWithFont:detailsLabel.font 
                                       constrainedToSize:kThrobberSize
                                           lineBreakMode:detailsLabel.lineBreakMode];
    detailsRect = CGRectMake(x, y+kThrobberDetailsY, w, detailsSize.height);
  }
  detailsLabel.frame = detailsRect;
  
  //
  
  CGFloat miny = CGFLOAT_MAX;
  CGFloat maxy = 0;
  for (UIView *view in contentView.subviews) {
    miny = MIN(miny, CGRectGetMinY(view.frame));
    maxy = MAX(maxy, CGRectGetMaxY(view.frame));
  }
  CGFloat delta = CGRectGetMidY(contentView.bounds) - 0.5 * (maxy + miny);
  for (UIView *view in contentView.subviews) {
    view.center = CGPointMake(view.center.x, view.center.y + delta);
  }
}

- (void)changeOrientationCallback:(NSNotification *)notification
{
  [self setNeedsLayout];
  [self setNeedsDisplay];
}

#pragma mark -

- (void)show
{
  [self.superview bringSubviewToFront:self];
  self.hidden = NO;
  self.alpha = 0.0;
  
  [UIView animateWithDuration:0.2 animations:^{
    self.alpha = 1.0;
  }];
}

- (void)dismiss
{
  [UIView animateWithDuration:0.2 animations:^{
    self.alpha = 0.0;
  } completion:^(BOOL finished) {
    if (finished)
      self.hidden = YES;
  }];
}

@end




