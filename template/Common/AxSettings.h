//
//  AxSettings.h
//

#import <Foundation/Foundation.h>
#import "SingletonMacro.h"


@interface AxSettings : NSObject
{
  BOOL  onLine;
}
@property(nonatomic, assign, getter=isOnLine) BOOL onLine;

+ (id)sharedInstance;

@end




