//
//  UIDevice+Platform.h
//
//  Created by Andrew Kovalev
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UIDevice (Platform)

- (NSString *)platform;
- (NSString *)platformString;
- (NSInteger)platformRAM;

@end





