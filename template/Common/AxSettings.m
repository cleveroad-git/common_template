//
//  AxSettings.h
//

#import "AxSettings.h"


@implementation AxSettings
@synthesize onLine;

SYNTHESIZE_SINGLETON_FOR_CLASS(AxSettings)

- (id)init {
  if ((self = [super init])) {
    /// Initialization code
    onLine = YES;
  }
  return self;
}

@end




