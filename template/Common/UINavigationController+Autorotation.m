//
//  UINavigationController+Autorotation.m
//  ChinesePoker
//
//  Created by Andrew Kovalev on 11/03/2012
//

#import "UINavigationController+Autorotation.h"


@implementation UINavigationController (Autorotation)

- (BOOL)shouldAutorotate {
  return [self.topViewController shouldAutorotate];
}

- (NSUInteger)supportedInterfaceOrientations {
  return [self.topViewController supportedInterfaceOrientations];
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
  return [self.topViewController preferredInterfaceOrientationForPresentation];
}

@end





