//
//  ChatPane.h
//  ChatKeyboard
//
//  Created by Gromov on 23.12.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, ButtonState) {
    ButtonStateMenu = 1,
    ButtonStateVoice,
    ButtonStateSend,
    ButtonStateKeyboard
};

@interface ChatPane : UIView {
    @public
    IBOutlet UITextView * inputTV;
    IBOutlet UIButton * sendButton;
    IBOutlet UIButton * moreButton;
    IBOutlet UIButton * emojeButton;
}

- (UITextView *)textView;
- (void)setInputView:(UIView *)inputView;
- (void)setMoreButtonState:(ButtonState)state;
- (void)setSendButtonState:(ButtonState)state;

@end
