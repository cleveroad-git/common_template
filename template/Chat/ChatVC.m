//
//  ChatVC.m
//  ChatKeyboard
//
//  Created by Gromov on 23.12.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "ChatVC.h"
#import "ChatPane.h"
#import "RecordAudioView.h"
#import "ActionLauncherView.h"
#import "CameraManager.h"
#import "STKStickerController.h"

@interface ChatVC () <ActionLauncherViewDelegate> {
    IBOutlet ChatPane * inputPanel;
    IBOutlet UITableView * mainChatTable;
    IBOutlet NSLayoutConstraint * bottomViewSpace;
    BOOL keyboardIsShown;
    CameraManager * cameraManager;
}

@property (strong, nonatomic) STKStickerController * stickerController;

@end

@implementation ChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    UIView * iav = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 0)];
//    [self.view addSubview:iav];
    inputPanel->inputTV.inputAccessoryView = iav;
//    [self.view addConstraint:[NSLayoutConstraint
//                         constraintWithItem:inputPanel
//                         attribute:NSLayoutAttributeBottom
//                         relatedBy:NSLayoutRelationEqual
//                         toItem:iav
//                         attribute:NSLayoutAttributeTop
//                         multiplier:1.0f
//                         constant:0.0f]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShowNotification:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHideNotification:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                      selector:@selector(keyboardDidChangeFrameNotification:)
                          name:UIKeyboardDidChangeFrameNotification
                        object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.view endEditing:YES];
    [super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [super viewDidDisappear:animated];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - ActionLauncherViewDelegate

- (void)performActionWithType:(LauncherViewActionType)type
{
    switch (type) {
        case LauncherViewActionRecordAudio:
            [self showRecordVoicePane:inputPanel->sendButton];
            break;
            
        case LauncherViewActionPhoto:
            [self showPhotoCamera];
            break;
            
        default:
            break;
    }
}

#pragma mark - Actions

- (IBAction)showMenuPane:(UIButton *)sender
{
    if (sender.tag == 321) {
        ActionLauncherView * rav = [ActionLauncherView createLauncherViewWithDelegate:self];
        [inputPanel setInputView:rav];
        [self reloadInputViews];
        [inputPanel setMoreButtonState:ButtonStateMenu];
    }
    else {
        [inputPanel setInputView:nil];
        [self reloadInputViews];
        [inputPanel setMoreButtonState:ButtonStateKeyboard];
    }
}

- (IBAction)showPhotoCamera {
    [[inputPanel textView] resignFirstResponder];
    cameraManager = [[CameraManager alloc] initWithParentController:self];
    [cameraManager showCamera];
}

- (void)showKeyboard
{
    [inputPanel setInputView:nil];
    [self reloadInputViews];
}

- (void)reloadInputViews {
    [inputPanel.textView reloadInputViews];
    if (!keyboardIsShown) {
        [inputPanel.textView becomeFirstResponder];
    }
}

- (IBAction)showRecordVoicePane:(UIButton *)sender
{
    if (sender.tag == 331) {
        RecordAudioView * rav = [RecordAudioView createRecordAudioView];
        [inputPanel setInputView:rav];
        [rav setupView];
        [self reloadInputViews];
        [inputPanel setSendButtonState:ButtonStateVoice];
    }
    else if (sender.tag == 332) {
        [inputPanel setInputView:nil];
        [self reloadInputViews];
        [inputPanel setSendButtonState:ButtonStateKeyboard];
    }
    else {
        [self sendMessage];
        [inputPanel setSendButtonState:ButtonStateKeyboard];
    }
}

- (void)sendMessage
{
    
}

- (IBAction)showStickersPane
{
    [inputPanel setInputView:self.stickerController.stickersView];
    [self reloadInputViews];
    [inputPanel setMoreButtonState:ButtonStateKeyboard];
}

- (void)hideStickersPane {
    [inputPanel setInputView:nil];
    [self reloadInputViews];
}

#pragma mark - NSNotificationCenter

- (void)keyboardWillShowNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    CGRect keyboardFrameBegin = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrameEnd = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (CGRectEqualToRect(keyboardFrameBegin, keyboardFrameEnd)) {
        return;
    }
    CGFloat height = keyboardFrameEnd.size.height;
    double animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    bottomViewSpace.constant = height;
    [UIView animateWithDuration:animationDuration delay:0.0f
                        options:UIViewAnimationOptionCurveEaseOut animations:^{
                            [self.view layoutIfNeeded];
                        } completion:nil];
}

- (void)keyboardDidChangeFrameNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    NSLog(@"keyboardDidChangeFrameNotification - %@",userInfo);
    CGRect begin = [userInfo[UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect end = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    float dif = fabsf(begin.origin.y - end.origin.y);
    
}

- (void)keyboardWillHideNotification:(NSNotification *)notification {
    NSDictionary *userInfo = notification.userInfo;
    CGRect keyboardFrameBegin = [[userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGRect keyboardFrameEnd = [[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (CGRectEqualToRect(keyboardFrameBegin, keyboardFrameEnd)) {
        return;
    }
    double animationDuration = [[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    bottomViewSpace.constant = 0;
    [UIView animateWithDuration:animationDuration animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
    }];
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *newText = textView.text;
    newText = [newText stringByReplacingCharactersInRange:range withString:text];
    if (newText.length) {
        [inputPanel setSendButtonState:ButtonStateSend];
    }
    else {
        [inputPanel setSendButtonState:ButtonStateKeyboard];
    }
    return YES;
}

#pragma mark - STKStickerControllerDelegate

- (void)stickerController:(STKStickerController *)stickerController didSelectStickerWithMessage:(NSString *)message {
    
}

#pragma mark - Property

- (STKStickerController *)stickerController {
    if (!_stickerController) {
        _stickerController = [STKStickerController new];
        _stickerController.delegate = self;
    }
    return _stickerController;
}

@end
