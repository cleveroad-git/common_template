//
//  ActionLauncherView.h
//  SENDER
//
//  Created by Eugene on 11/3/14.
//  Copyright (c) 2014 PrivatBank. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LauncherViewActionType) {
    LauncherViewActionRecordAudio = 1,
    LauncherViewActionPhoto,
    LauncherViewActionVideo
};

@protocol ActionLauncherViewDelegate <NSObject>

- (void)performActionWithType:(LauncherViewActionType)type;

@end

@interface ActionLauncherView : UIView

@property (nonatomic, strong) IBOutlet UIButton * sendButton;
@property (nonatomic, strong) IBOutlet UIButton * receiveButton;
@property (nonatomic, strong) IBOutlet UIButton * addButton;

@property (nonatomic, strong) id<ActionLauncherViewDelegate> delegate;

+ (ActionLauncherView *)createLauncherViewWithDelegate:(id<ActionLauncherViewDelegate>)delegate;

@end
