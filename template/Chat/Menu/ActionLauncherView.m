//
//  ActionLauncherView.m
//  SENDER
//
//  Created by Eugene on 11/3/14.
//  Copyright (c) 2014 PrivatBank. All rights reserved.
//

#import "ActionLauncherView.h"


@implementation ActionLauncherView

+ (ActionLauncherView *)createLauncherViewWithDelegate:(id<ActionLauncherViewDelegate>)delegate
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ActionLauncherView" owner:self options:nil];
    
    if (nib.count > 0) {
        ActionLauncherView * view = [nib objectAtIndex:0];
        view.frame = (CGRect){view.frame.origin, CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetHeight(view.frame))};
        view.delegate = delegate;
        return view;
    }
    return nil;
}

- (IBAction)pushAction:(id)sender
{
    [_delegate performActionWithType:((UIButton *)sender).tag];
}

@end
