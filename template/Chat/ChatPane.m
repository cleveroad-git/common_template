//
//  ChatPane.m
//  ChatKeyboard
//
//  Created by Gromov on 23.12.15.
//  Copyright © 2015 cleveroad. All rights reserved.
//

#import "ChatPane.h"

@implementation ChatPane

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (UITextView *)textView
{
    return inputTV;
}

- (void)setInputView:(UIView *)inputView
{
    inputTV.inputView = inputView;
}

- (void)setMoreButtonState:(ButtonState)state
{
    if (state == ButtonStateKeyboard) {
        [moreButton setImage:[UIImage imageNamed:@"more.png"] forState:UIControlStateNormal];
        moreButton.tag = 321;
    }
    else {
        [moreButton setImage:[UIImage imageNamed:@"more_selected.png"] forState:UIControlStateNormal];
        moreButton.tag = 322;
    }
}

- (void)setSendButtonState:(ButtonState)state
{
    if (state == ButtonStateKeyboard) {
        [sendButton setImage:[UIImage imageNamed:@"ic_send_voice"] forState:UIControlStateNormal];
        sendButton.tag = 331;
    }
    else if (state == ButtonStateVoice) {
        [sendButton setImage:[UIImage imageNamed:@"kbd"] forState:UIControlStateNormal];
        sendButton.tag = 332;
    }
    else {
        [sendButton setImage:[UIImage imageNamed:@"play"] forState:UIControlStateNormal];
        sendButton.tag = 333;
    }
}

@end
