//
//  RecordAudioView.m
//
//
//
//
//

#import "RecordAudioView.h"
//#import "FileManager.h"

@implementation RecordAudioView


+ (RecordAudioView *)createRecordAudioView
{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"RecordAudioView" owner:self options:nil];
    
    if (nib.count > 0) {
        RecordAudioView * view = [nib objectAtIndex:0];
        view.frame = (CGRect){view.frame.origin, CGSizeMake([UIScreen mainScreen].bounds.size.width, CGRectGetHeight(view.frame))};
        return view;
    }
    return nil;
}

- (void)dealloc
{
    recorder = nil;
}

- (void)setupView
{
    timerCount = 0;
    [self.pLabel setBackBorderWidth: 4.0];
    [self.pLabel setFrontBorderWidth: 9.0];
    [self.pLabel setColorTable: @{
                                  NSStringFromProgressLabelColorTableKey(ProgressLabelTrackColor):[UIColor clearColor],
                                  NSStringFromProgressLabelColorTableKey(ProgressLabelProgressColor):[UIColor blueColor]
                                  }];
    
    [self.startStopButton setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    
    [self.playButton setImage:[UIImage imageNamed:@"play.png"] forState:UIControlStateNormal];
    
    recImage = [UIImage imageNamed:@"hold&talk_press.png"];
    stopImage = [UIImage imageNamed:@"hold&talk_normal.png"];
    [self changeRecBgImage:NO];
    [self showHideRecButtons:NO];
}

- (void)updateCount {
    
    timerCount += 0.015;
    [self.pLabel setProgress:timerCount];
}

- (IBAction)sendRecordToServer:(id)sender
{
    [self changeRecBgImage:NO];
    [self showHideRecButtons:NO];


    timerCount = 0;
    [self.pLabel setProgress:0];
   
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        BOOL res = [recorder convertToMp3];
        if (res) {
//            TO DO
        }
        recorder = nil;
    });
}

- (IBAction)cancelRecord:(id)sender
{
    [self changeRecBgImage:NO];
    [self showHideRecButtons:NO];
    
    timerCount = 0;
    [self.pLabel setProgress:0];
    recorder = nil;
}

- (IBAction)startRecordAudio:(id)sender
{
    timerCount = 0;
    [self.pLabel setProgress:0];
    self.sendButton.hidden = YES;
    self.playButton.hidden = YES;
    
    recorder = [[AudioRecorder alloc] init];
    [recorder startRecord];
    [self changeRecBgImage:YES];
    
    repeatTimer = [NSTimer scheduledTimerWithTimeInterval:0.5
                                                 target:self
                                               selector:@selector(updateCount)
                                               userInfo:nil
                                                repeats:YES];
    mainTimer = [NSTimer scheduledTimerWithTimeInterval:29.5
                                                 target:self
                                               selector:@selector(stopRecording:)
                                               userInfo:nil
                                                repeats:NO];
}

- (void)stopRepeatTimer
{
    [repeatTimer invalidate];
    repeatTimer = nil;
    durationOfRecordedFile = [recorder getFileDuration];
    [self changeRecBgImage:NO];
    if (durationOfRecordedFile < 1) {
        [self.pLabel setProgress:0];
        [self showHideRecButtons:NO];
        return;
    }

    [self showHideRecButtons:YES];
    durationOfRecordedFileLabel.text = [NSString stringWithFormat:@"%.02f",durationOfRecordedFile];
//    [FileManager sharedFileManager].lastRecorderAudioDuration = [NSString /stringWithFormat:@"%f",durationOfRecordedFile];
}

- (IBAction)playRecordedAudio:(id)sender
{
    if ([recorder playerStatus]) {
        [recorder stopPlay];
        [self.playButton setImage:[UIImage imageNamed:@"play.png"]
                         forState:UIControlStateNormal];
        [playTimer invalidate];
        playTimer = nil;
        return;
    }

    if ([recorder playWithDelegate:self])
    {
        [self.playButton setImage:[UIImage imageNamed:@"pause.png"]
                         forState:UIControlStateNormal];
        
        playTimer = [NSTimer
                      scheduledTimerWithTimeInterval:0.1
                      target:self selector:@selector(timerFired:)
                      userInfo:nil repeats:YES];
    }
}

- (void)timerFired:(NSTimer*)timer
{
    [self updateDisplay];
}

- (void)updateDisplay
{
    NSTimeInterval currentTime = [recorder currentTime];
    durationOfRecordedFileLabel.text = [NSString stringWithFormat:@"%.02f", currentTime];
}

#pragma mark - AVAudioPlayerDelegate

- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
    [playTimer invalidate];
    playTimer = nil;
    [self.playButton setImage:[UIImage imageNamed:@"play.png"]
                     forState:UIControlStateNormal];
    durationOfRecordedFileLabel.text = [NSString stringWithFormat:@"%.02f",durationOfRecordedFile];
}

- (IBAction)stopRecording:(id)sender
{
    [recorder stopRecord];
    [self stopRepeatTimer];
}

- (void)changeRecBgImage:(BOOL)mode
{
    if (mode) {
        recBgImageView.image = recImage;
    }
    else {
        recBgImageView.image = stopImage;
    }
}

- (void)showHideRecButtons:(BOOL)mode
{
    self.pLabel.hidden = mode;
    self.startStopButton.hidden = mode;
    recBgImageView.hidden = mode;
    
    durationOfRecordedFileLabel.hidden = !mode;
    self.sendButton.hidden = !mode;
    self.playButton.hidden = !mode;
    self.cancelButton.hidden = !mode;
    playBgImageView.hidden = !mode;
    self.playButton.hidden = !mode;
}

@end
