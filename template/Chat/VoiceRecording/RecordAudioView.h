//
//  RecordAudioView.h
//
//
//
//
//

#import <UIKit/UIKit.h>
#import "KAProgressLabel.h"
#import "AudioRecorder.h"

@interface RecordAudioView : UIView <AVAudioPlayerDelegate>
{
    float timerCount;
    NSTimer * mainTimer;
    NSTimer * repeatTimer;
    NSTimer * playTimer;
    AudioRecorder * recorder;
    IBOutlet UIImageView * recBgImageView;
    IBOutlet UIImageView * playBgImageView;
    IBOutlet UILabel * durationOfRecordedFileLabel;
    UIImage * recImage;
    UIImage * stopImage;
    float durationOfRecordedFile;
}

@property (nonatomic, strong) IBOutlet UIButton * startStopButton;
@property (nonatomic, strong) IBOutlet UIButton * sendButton;
@property (nonatomic, strong) IBOutlet UIButton * cancelButton;
@property (nonatomic, strong) IBOutlet UIButton * playButton;

@property (nonatomic, weak) IBOutlet KAProgressLabel * pLabel;

+ (RecordAudioView *)createRecordAudioView;

- (void)setupView;

@end
