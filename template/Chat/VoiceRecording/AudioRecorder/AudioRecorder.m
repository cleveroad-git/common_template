//
//  AudioRecorder.m
//  SENDER
//
//  Created by Nick Gromov on 10/24/14.
//  Copyright (c) 2014 PrivatBank. All rights reserved.
//

#import "AudioRecorder.h"
#include "lame.h"

@interface AudioRecorder () {
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
    NSURL * outputFileURL;
    NSString * audioFilePath;
    CGFloat sampleRate;
}
- (void)setAudioFileDomain;
- (NSURL *)getAudioFileUrl;
- (NSString *)getAudioFilePath;
@end


@implementation AudioRecorder

- (id)init
{
    self = [super init];
    if(self)
    {
        // Setup audio session
        AVAudioSession *session = [AVAudioSession sharedInstance];
        [session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
        sampleRate  = 20050;
        // Define the recorder setting
        NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
        
        [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];
        [recordSetting setValue:[NSNumber numberWithFloat:sampleRate] forKey:AVSampleRateKey];
        [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
        [recordSetting setValue:[NSNumber numberWithInt:AVAudioQualityLow] forKey:AVEncoderAudioQualityKey];
        
        // Initiate and prepare the recorder
        recorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:[self getTemporaryDirectory]] settings:recordSetting error:nil];
        recorder.delegate = self;
        recorder.meteringEnabled = YES;
        [recorder prepareToRecord];
    }
    return self;
}

- (NSURL *)getAudioFileUrl
{
    if (!outputFileURL) {
        [self setAudioFileDomain];
    }
    return outputFileURL;
}

- (NSString *)getAudioFilePath
{
    if (!audioFilePath) {
        [self setAudioFileDomain];
    }
    return audioFilePath;
}

- (void)setAudioFileDomain
{
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.mp3",
                               nil];
    outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    audioFilePath = [[pathComponents[0] stringByAppendingString:@"/"] stringByAppendingString:pathComponents[1]];
}

- (NSString *)getTemporaryDirectory
{
    return [NSTemporaryDirectory() stringByAppendingString:@"RecordedFile"];
}

- (void)startRecord
{
    NSError * error = nil;
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setActive:YES error:&error];
    
    if (!error) {
        NSLog(@"START RECORD");
        [recorder record];
    }
}

- (void)stopRecord
{
    [recorder stop];
    NSLog(@"STOP RECORD");
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    NSError * error;
    [audioSession setActive:NO error:&error];
//    [self performSelector:@selector(playTapped) withObject:nil afterDelay:1];
}

- (void)playAudioFormUrl:(NSString *)fileURL
{
    
}

- (BOOL)playWithDelegate:(id<AVAudioPlayerDelegate>)delegate {
    if (!recorder.recording){
        NSError * error;
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[self getTemporaryDirectory]] error:&error];
        if (!error)
        {
            UInt32 doChangeDefaultRoute = 1;
            
            AudioSessionSetProperty (
                                     kAudioSessionProperty_OverrideCategoryDefaultToSpeaker,
                                     sizeof (doChangeDefaultRoute),
                                     &doChangeDefaultRoute);
            [player setDelegate:delegate];

            player.volume = 1;
            [player play];
            return YES;
        }
    }
    return NO;
}

- (BOOL)playWithDelegate:(id<AVAudioPlayerDelegate>)delegate fromPath:(NSURL *)filePath
{
    if (!recorder.recording){
        NSError * error;
        player = [[AVAudioPlayer alloc] initWithContentsOfURL:filePath error:&error];
        if (!error)
        {
            UInt32 doChangeDefaultRoute = 1;
            
            AudioSessionSetProperty (
                                     kAudioSessionProperty_OverrideCategoryDefaultToSpeaker,
                                     sizeof (doChangeDefaultRoute),
                                     &doChangeDefaultRoute);
            [player setDelegate:delegate];
            
            player.volume = 1;
            [player play];
            return YES;
        }
    }
    return NO;
}

- (float)getFileDuration
{
    NSError * error;
    AVAudioPlayer * player1 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[self getTemporaryDirectory]] error:&error];
    float duration = player1.duration;
    player1 = nil;
    return duration;
}

- (BOOL)playerStatus
{
    return player.playing;
}

- (void)stopPlay
{
    [player stop];
}

#pragma mark - convertToMp3

//- (BOOL)convertToMp3
//{
//    // FIX FOR RUN ON SIMULATOR!!!!!!
//    return NO;
//}


- (BOOL)convertToMp3
{
    // UNLOCK ON PRODUCTION !!!!!!
    NSString *cafFilePath = [self getTemporaryDirectory];
    
    NSString * mp3FilePath = [self getAudioFilePath];
    
    @try {
        int read, write;
        
        FILE *pcm = fopen([cafFilePath cStringUsingEncoding:1], "rb");  //source
        fseek(pcm, 4*1024, SEEK_CUR);                                   //skip file header
        FILE *mp3 = fopen([mp3FilePath cStringUsingEncoding:1], "wb");  //output
        
        const int PCM_SIZE = 8192;
        const int MP3_SIZE = 8192;
        short int pcm_buffer[PCM_SIZE*2];
        unsigned char mp3_buffer[MP3_SIZE];
        
        lame_t lame = lame_init();
        lame_set_in_samplerate(lame, sampleRate);
        lame_set_VBR(lame, vbr_default);
        lame_init_params(lame);
        
        do {
            read = fread(pcm_buffer, 2*sizeof(short int), PCM_SIZE, pcm);
            if (read == 0)
                write = lame_encode_flush(lame, mp3_buffer, MP3_SIZE);
            else
                write = lame_encode_buffer_interleaved(lame, pcm_buffer, read, mp3_buffer, MP3_SIZE);
            
            fwrite(mp3_buffer, write, 1, mp3);
            
        } while (read != 0);
        
        lame_close(lame);
        fclose(mp3);
        fclose(pcm);
        return YES;
    }
    @catch (NSException *exception) {
        NSLog(@"%@",[exception description]);
        return NO;
    }
}
    

    
    
    
//    @finally {
//        [self performSelectorOnMainThread:@selector(convertMp3Finish)
//                               withObject:nil
//                            waitUntilDone:YES];
//    }


- (NSTimeInterval)currentTime;
{
    return player.currentTime;
}

//#pragma mark - AVAudioRecorderDelegate
//
//- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)avrecorder successfully:(BOOL)flag{
//    
//}

//#pragma mark - AVAudioPlayerDelegate
//
//- (void) audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
//    
//}

#pragma mark - manage file

- (NSData *)getFileData
{
    return [NSData dataWithContentsOfFile:[self getAudioFilePath]];
}

- (void)deleteFile
{
    NSError *error;
    BOOL success = [[NSFileManager defaultManager] removeItemAtPath:[self getAudioFilePath] error:&error];
    if (!success) {
        NSLog(@"Error removing document path: %@", error.localizedDescription);
    }
}

@end
