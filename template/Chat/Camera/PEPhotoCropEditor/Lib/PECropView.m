//
//  PECropView.m
//  PhotoCropEditor
//
//  Created by kishikawa katsumi on 2013/05/19.
//  Copyright (c) 2013 kishikawa katsumi. All rights reserved.
//

#import "PECropView.h"
#import "PECropRectView.h"
#import "UIImage+PECrop.h"

static const CGFloat MarginTop = 37.0f;
static const CGFloat MarginBottom = MarginTop;
static const CGFloat MarginLeft = 20.0f;
static const CGFloat MarginRight = MarginLeft;

CGFloat DegreesToRadians(CGFloat degrees) {return degrees * M_PI / 180;};

@interface PECropView () <UIScrollViewDelegate, UIGestureRecognizerDelegate, PECropRectViewDelegate> {
    UIView * top;
    BOOL needChangeAnchorPoint;
    BOOL isRotating;
    int n;
    NSNumber * zooming;
}

@property (nonatomic) UIScrollView *scrollView;
@property (nonatomic) UIView *zoomingView;
@property (nonatomic) UIImageView *imageView;

@property (nonatomic) PECropRectView *cropRectView;
@property (nonatomic) UIView *topOverlayView;
@property (nonatomic) UIView *leftOverlayView;
@property (nonatomic) UIView *rightOverlayView;
@property (nonatomic) UIView *bottomOverlayView;

@property (nonatomic) CGRect insetRect;
@property (nonatomic) CGRect editingRect;

@property (nonatomic, getter = isResizing) BOOL resizing;
@property (nonatomic) UIInterfaceOrientation interfaceOrientation;

@end

@implementation PECropView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self commonInit];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}

- (void)commonInit
{
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.backgroundColor = [UIColor clearColor];
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
    self.scrollView.delegate = self;
    self.scrollView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.scrollView.maximumZoomScale = 20.0f;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.showsVerticalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    self.scrollView.bouncesZoom = NO;
    self.scrollView.clipsToBounds = NO;
    [self addSubview:self.scrollView];
    
    //    UIRotationGestureRecognizer *rotationGestureRecognizer = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotation:)];
    //    rotationGestureRecognizer.delegate = self;
    //    [self.scrollView addGestureRecognizer:rotationGestureRecognizer];
    
    self.cropRectView = [[PECropRectView alloc] init];
    self.cropRectView.delegate = self;
    [self addSubview:self.cropRectView];
    
    self.topOverlayView = [[UIView alloc] init];
    self.topOverlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    [self addSubview:self.topOverlayView];
    
    self.leftOverlayView = [[UIView alloc] init];
    self.leftOverlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    [self addSubview:self.leftOverlayView];
    
    self.rightOverlayView = [[UIView alloc] init];
    self.rightOverlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    [self addSubview:self.rightOverlayView];
    
    self.bottomOverlayView = [[UIView alloc] init];
    self.bottomOverlayView.backgroundColor = [UIColor colorWithWhite:0.0f alpha:0.4f];
    [self addSubview:self.bottomOverlayView];
}

#pragma mark -

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    if (!self.userInteractionEnabled) {
        return nil;
    }
    
    UIView *hitView = [self.cropRectView hitTest:[self convertPoint:point toView:self.cropRectView] withEvent:event];
    if (hitView) {
        return hitView;
    }
    CGPoint locationInImageView = [self convertPoint:point toView:self.zoomingView];
    CGPoint zoomedPoint = CGPointMake(locationInImageView.x * self.scrollView.zoomScale, locationInImageView.y * self.scrollView.zoomScale);
    if (CGRectContainsPoint(self.zoomingView.frame, zoomedPoint)) {
        return self.scrollView;
    }
    
    return [super hitTest:point withEvent:event];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!self.image) {
        return;
    }
    
    UIInterfaceOrientation interfaceOrientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
        self.editingRect = CGRectInset(self.bounds, MarginLeft, MarginTop);
    } else {
        self.editingRect = CGRectInset(self.bounds, MarginLeft, MarginLeft);
    }
    
    if (!self.imageView) {
        if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
            self.insetRect = CGRectInset(self.bounds, MarginLeft, MarginTop);
        } else {
            self.insetRect = CGRectInset(self.bounds, MarginLeft, MarginLeft);
        }
        
        [self setupImageView];
    }
    
    if (!self.isResizing) {
        [self layoutCropRectViewWithCropRect:self.scrollView.frame];
        
        if (self.interfaceOrientation != interfaceOrientation) {
            [self zoomToCropRect:self.scrollView.frame];
        }
    }
    
    self.interfaceOrientation = interfaceOrientation;
}

- (void)layoutCropRectViewWithCropRect:(CGRect)cropRect
{
    self.cropRectView.frame = cropRect;
    //    NSLog(@"after cropRectView - x: %f y: %f w: %f h:%f",cropRect.origin.x ,cropRect.origin.y , cropRect.size.width , cropRect.size.height);
    [self layoutOverlayViewsWithCropRect:cropRect];
}

- (void)layoutOverlayViewsWithCropRect:(CGRect)cropRect
{
    self.topOverlayView.frame = CGRectMake(0.0f,
                                           0.0f,
                                           CGRectGetWidth(self.bounds),
                                           CGRectGetMinY(cropRect));
    self.leftOverlayView.frame = CGRectMake(0.0f,
                                            CGRectGetMinY(cropRect),
                                            CGRectGetMinX(cropRect),
                                            CGRectGetHeight(cropRect));
    self.rightOverlayView.frame = CGRectMake(CGRectGetMaxX(cropRect),
                                             CGRectGetMinY(cropRect),
                                             CGRectGetWidth(self.bounds) - CGRectGetMaxX(cropRect),
                                             CGRectGetHeight(cropRect));
    self.bottomOverlayView.frame = CGRectMake(0.0f,
                                              CGRectGetMaxY(cropRect),
                                              CGRectGetWidth(self.bounds),
                                              CGRectGetHeight(self.bounds) - CGRectGetMaxY(cropRect));
}

- (void)setupImageView
{
    CGRect cropRect = AVMakeRectWithAspectRatioInsideRect(self.image.size, self.insetRect);
    
    self.scrollView.frame = cropRect;
    self.scrollView.contentSize = cropRect.size;
    
    self.zoomingView = [[UIView alloc] initWithFrame:self.scrollView.bounds];
    self.zoomingView.backgroundColor = [UIColor clearColor];
    [self.scrollView addSubview:self.zoomingView];
    
    self.imageView = [[UIImageView alloc] initWithFrame:self.zoomingView.bounds];
    self.imageView.backgroundColor = [UIColor clearColor];
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
    self.imageView.image = self.image;
    [self.zoomingView addSubview:self.imageView];
}

#pragma mark - Setters/Getters

- (void)setImage:(UIImage *)image
{
    _image = image;
    
    [self.imageView removeFromSuperview];
    self.imageView = nil;
    
    [self.zoomingView removeFromSuperview];
    self.zoomingView = nil;
    
    [self setNeedsLayout];
    n = 1;
}

- (void)setKeepingCropAspectRatio:(BOOL)keepingCropAspectRatio
{
    _keepingCropAspectRatio = keepingCropAspectRatio;
    self.cropRectView.keepingAspectRatio = self.keepingCropAspectRatio;
}

- (void)setCropAspectRatio:(CGFloat)aspectRatio andCenter:(BOOL)center
{
    CGRect cropRect = self.scrollView.frame;
    CGFloat width = CGRectGetWidth(cropRect);
    CGFloat height = CGRectGetHeight(cropRect);
    if (aspectRatio <= 1.0f) {
        width = height * aspectRatio;
        if (width > CGRectGetWidth(self.imageView.bounds)) {
            width = CGRectGetWidth(cropRect);
            height = width / aspectRatio;
        }
    } else {
        height = width / aspectRatio;
        if (height > CGRectGetHeight(self.imageView.bounds)) {
            height = CGRectGetHeight(cropRect);
            width = height * aspectRatio;
        }
    }
    cropRect.size = CGSizeMake(width, height);
    [self zoomToCropRect:cropRect andCenter:center];
}

- (void)setCropAspectRatio:(CGFloat)aspectRatio
{
    [self setCropAspectRatio:aspectRatio andCenter:YES];
}

- (CGFloat)cropAspectRatio
{
    CGRect cropRect = self.scrollView.frame;
    CGFloat width = CGRectGetWidth(cropRect);
    CGFloat height = CGRectGetHeight(cropRect);
    return width / height;
}

- (void)setCropRect:(CGRect)cropRect
{
    [self zoomToCropRect:cropRect];
}

- (CGRect)cropRect
{
    return self.scrollView.frame;
}

- (void)setImageCropRect:(CGRect)imageCropRect
{
    [self resetCropRect];
    
    CGRect scrollViewFrame = self.scrollView.frame;
    CGSize imageSize = self.image.size;
    
    CGFloat scale = MIN(CGRectGetWidth(scrollViewFrame) / imageSize.width,
                        CGRectGetHeight(scrollViewFrame) / imageSize.height);
    
    CGFloat x = CGRectGetMinX(imageCropRect) * scale + CGRectGetMinX(scrollViewFrame);
    CGFloat y = CGRectGetMinY(imageCropRect) * scale + CGRectGetMinY(scrollViewFrame);
    CGFloat width = CGRectGetWidth(imageCropRect) * scale;
    CGFloat height = CGRectGetHeight(imageCropRect) * scale;
    
    CGRect rect = CGRectMake(x, y, width, height);
    CGRect intersection = CGRectIntersection(rect, scrollViewFrame);
    
    if (!CGRectIsNull(intersection)) {
        self.cropRect = intersection;
    }
}

- (void)resetCropRect
{
    [self resetCropRectAnimated:NO];
}

- (void)resetCropRectAnimated:(BOOL)animated
{
    if (animated) {
        [UIView beginAnimations:nil context:NULL];
        [UIView setAnimationDuration:0.25];
        [UIView setAnimationBeginsFromCurrentState:YES];
    }
    
    self.imageView.transform = CGAffineTransformIdentity;
    
    CGSize contentSize = self.scrollView.contentSize;
    CGRect initialRect = CGRectMake(0.0f, 0.0f, contentSize.width, contentSize.height);
    [self.scrollView zoomToRect:initialRect animated:NO];
    
    self.scrollView.bounds = self.imageView.bounds;
    
    [self layoutCropRectViewWithCropRect:self.scrollView.bounds];
    
    if (animated) {
        [UIView commitAnimations];
    }
}

- (UIImage *)croppedImage
{
    return [self.image rotatedImageWithtransform:self.rotation croppedToRect:self.zoomedCropRect];
}

- (CGRect)zoomedCropRect
{
    CGRect cropRect = [self convertRect:self.scrollView.frame toView:self.zoomingView];
    CGSize size = self.image.size;
    
    CGFloat ratio = 1.0f;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad || UIInterfaceOrientationIsPortrait(orientation)) {
        ratio = CGRectGetWidth(AVMakeRectWithAspectRatioInsideRect(self.image.size, self.insetRect)) / size.width;
    } else {
        ratio = CGRectGetHeight(AVMakeRectWithAspectRatioInsideRect(self.image.size, self.insetRect)) / size.height;
    }
    
    CGRect zoomedCropRect = CGRectMake(cropRect.origin.x / ratio,
                                       cropRect.origin.y / ratio,
                                       cropRect.size.width / ratio,
                                       cropRect.size.height / ratio);
    
    return zoomedCropRect;
}

- (BOOL)userHasModifiedCropArea
{
    CGRect zoomedCropRect = CGRectIntegral(self.zoomedCropRect);
    return (!CGPointEqualToPoint(zoomedCropRect.origin, CGPointZero) ||
            !CGSizeEqualToSize(zoomedCropRect.size, self.image.size) ||
            !CGAffineTransformEqualToTransform(self.rotation, CGAffineTransformIdentity));
}

- (CGAffineTransform)rotation
{
    return self.scrollView.transform;
}

- (CGFloat)rotationAngle
{
    CGAffineTransform rotation = self.scrollView.transform;
    return atan2f(rotation.b, rotation.a);
}

- (CGFloat)scale {
    CGAffineTransform t = self.scrollView.transform;
    return sqrt(t.a * t.a + t.c * t.c);
}

- (void)setRotationAngle:(CGFloat)rotationAngle
{
    if (isRotating) {
        return;
    }
    isRotating = YES;
    float editingRectRatio1 = CGRectGetWidth(self.editingRect)/CGRectGetHeight(self.cropRectView.frame);
    float editingRectRatio2 = CGRectGetHeight(self.editingRect)/CGRectGetWidth(self.cropRectView.frame);
    float editingRectRatio = editingRectRatio1 > editingRectRatio2 ? editingRectRatio2 : editingRectRatio1;
    float currentScale = [self scale];
    
    CGAffineTransform rotation = CGAffineTransformMakeRotation(rotationAngle);
    //    CGAffineTransform zoom = n%2 ? CGAffineTransformMakeScale(editingRectRatio, editingRectRatio) : CGAffineTransformMakeScale(1.0, 1.0);
    CGAffineTransform zoom = CGAffineTransformMakeScale(editingRectRatio * currentScale, editingRectRatio * currentScale);
    CGAffineTransform transform = CGAffineTransformConcat(zoom, rotation);
    _cropRectView.alpha = 0.0;
    [UIView animateWithDuration:0.2 animations:^{
        self.scrollView.transform = transform;
    } completion:^(BOOL finished) {
        CGRect rect = self.imageView.frame;
        NSLog(@"%@", self.imageView);
        self.resizing = YES;
        [self layoutCropRectViewWithCropRect:self.scrollView.frame];
        _cropRectView.alpha = 1.0;
        needChangeAnchorPoint = NO;
        isRotating = NO;
    }];
    ++n;
}

//- (void)resetZoomingViewStartState
//{
//    self.zoomingView.transform = CGAffineTransformIdentity;
//    self.zoomingView.frame = (CGRect){CGPointMake(0.0, 0.0), self.zoomingView.frame.size};
//    self.scrollView.frame = AVMakeRectWithAspectRatioInsideRect(self.image.size, self.insetRect);
//    self.scrollView.contentSize = CGSizeMake(0.0, 0.0);
//}

- (CGRect)pseudoRotateCropRect:(float)ratio
{
    float newH = CGRectGetWidth(self.cropRectView.frame)*ratio;
    float newW = CGRectGetHeight(self.cropRectView.frame)*ratio;
    float newY = (CGRectGetHeight(self.cropRectView.frame) - newH)/2 + self.cropRectView.frame.origin.y;
    float newX = (CGRectGetWidth(self.cropRectView.frame) - newW)/2 + self.cropRectView.frame.origin.x;
    return CGRectMake(newX, newY, newW, newH);
}

- (void)resetMinimumZoomScale
{
    CGSize boundsSize  = self.scrollView.bounds.size;
    CGSize size = self.imageView.frame.size;
    self.scrollView.minimumZoomScale = MAX(boundsSize.width/size.width, boundsSize.height/size.height);
}

//- (void)resetAnchorPoint
//{
//    if (!needChangeAnchorPoint) {
//        return;
//    }
//    //    CGPoint center = (CGPoint){CGRectGetWidth(_cropRectView.frame)/2, CGRectGetHeight(_cropRectView.frame)/2};
//    //    CGPoint centerInImageView = [_zoomingView convertPoint:center fromView:_cropRectView];
//    CGRect rectt = [_scrollView convertRect:_cropRectView.bounds fromView:_cropRectView];
//    CGRect rectt2 = [_scrollView convertRect:_scrollView.bounds fromView:_scrollView];
//    CGPoint centerInImageView = CGPointMake(rectt.origin.x + CGRectGetWidth(rectt)/2, rectt.origin.y + CGRectGetHeight(rectt)/2);
//    CGPoint ap = CGPointMake(centerInImageView.x / rectt2.size.width, centerInImageView.y / rectt2.size.height);
//    CGRect old = _scrollView.frame;
//    _scrollView.layer.anchorPoint =  ap;
//    _scrollView.frame = old;
//    //        if (top) {
//    //            [top removeFromSuperview];
//    //        }
//    //        top = [[UIView alloc] initWithFrame:CGRectMake(centerInImageView.x, centerInImageView.y, 4.0, 4.0)];
//    //        top.backgroundColor = [UIColor redColor];
//    //        [_imageView addSubview:top];
//}

//- (void)resetAnchorPoint
//{
//    CGPoint center = (CGPoint){CGRectGetWidth(_cropRectView.frame)/2, CGRectGetHeight(_cropRectView.frame)/2};
//    CGPoint centerInImageView = [_imageView convertPoint:center fromView:_cropRectView];
//    centerInImageView = CGPointMake(centerInImageView.x - _imageView.frame.origin.x, centerInImageView.y - _imageView.frame.origin.y);
//    CGPoint ap = CGPointMake(centerInImageView.x / _imageView.frame.size.width, centerInImageView.y / _imageView.frame.size.height);
//    CGRect old = _imageView.frame;
//    _imageView.layer.anchorPoint = ap;
//    _imageView.frame = old;
////    if (top) {
////        [top removeFromSuperview];
////    }
////    top = [[UIView alloc] initWithFrame:CGRectMake(centerInImageView.x, centerInImageView.y, 4.0, 4.0)];
////    top.backgroundColor = [UIColor redColor];
////    [_imageView addSubview:top];
//}

- (void)setRotationAngle:(CGFloat)rotationAngle snap:(BOOL)snap
{
    if (snap)
    {
        rotationAngle = nearbyintf(rotationAngle / M_PI_2) * M_PI_2;
    }
    self.rotationAngle = rotationAngle;
}

- (CGRect)cappedCropRectInImageRectWithCropRectView:(PECropRectView *)cropRectView
{
    CGRect cropRect = cropRectView.frame;
    
    CGRect rect = [self convertRect:cropRect toView:self.scrollView];
    CGRect zoomingViewRect = [self.scrollView convertRect:self.zoomingView.frame toView:self];
    
    int angle = [self rotationAngle];
    BOOL isrot = angle == 1 || angle == -1;
    
    if (CGRectGetMinX(rect) < CGRectGetMinX(self.zoomingView.frame)) {
        if (angle == 0) {
            float maxX = CGRectGetMaxX(cropRect);
            cropRect.origin.x = CGRectGetMinX(zoomingViewRect);
            CGFloat cappedWidth = maxX - cropRect.origin.x;
            cropRect.size = CGSizeMake(cappedWidth,
                                       !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
        else if (angle == 1) {
            float newOriginY = CGRectGetMinY([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            CGFloat cappedHeight =  CGRectGetHeight(cropRect) - (newOriginY - cropRect.origin.y);
            cropRect.origin.y = newOriginY;
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
        else if (angle == -1) {
            CGFloat cappedHeight =  CGRectGetMaxY([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinY(cropRect);
            
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
        else if (angle == -3 || angle == 3) {
            CGFloat cappedWidth = CGRectGetMaxX([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinX(cropRect);
            cropRect.size = CGSizeMake(cappedWidth,
                                       !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
    }
    if (CGRectGetMinY(rect) < CGRectGetMinY(self.zoomingView.frame)) {
        if (angle == 0) {
//            cropRect.origin.y = CGRectGetMinY([self.scrollView convertRect:self.zoomingView.frame toView:self]);
//            CGFloat cappedHeight =  CGRectGetMaxY(rect);
            float newOriginY = CGRectGetMinY([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            CGFloat cappedHeight =  CGRectGetHeight(cropRect) - (newOriginY - cropRect.origin.y);
            cropRect.origin.y = newOriginY;
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
        else if (angle == 1) {
            CGFloat cappedWidth =  CGRectGetMaxX([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinX(cropRect);
            cropRect.size = CGSizeMake(cappedWidth, !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
        else if (angle == -1) {
            float newOriginX = CGRectGetMinX([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            
            CGFloat cappedWidth = CGRectGetWidth(cropRect) - (newOriginX - cropRect.origin.x);
            cropRect.origin.x = newOriginX;
            cropRect.size = CGSizeMake(cappedWidth,
                                       !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
        else if (angle == -3 || angle == 3) {
            CGFloat cappedHeight =  CGRectGetMaxY([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinY(cropRect);
            
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
    }
    if (CGRectGetMaxX(rect) > CGRectGetMaxX(self.zoomingView.frame)) {
        if (angle == 0) {
            CGFloat cappedWidth = CGRectGetMaxX([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinX(cropRect);
            cropRect.size = CGSizeMake(cappedWidth,
                                       !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
        else if (angle == 1) {
            CGFloat cappedHeight =  CGRectGetMaxY([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinY(cropRect);
            
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
        else if (angle == -1) {
            float newOriginY = CGRectGetMinY([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            
            //            cropRect.origin.y = CGRectGetMinY([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            CGFloat cappedHeight =  CGRectGetHeight(cropRect) - (newOriginY - cropRect.origin.y);
            cropRect.origin.y = newOriginY;
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
        else if (angle == -3 || angle == 3) {
            float newOriginX = CGRectGetMinX([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            
            CGFloat cappedWidth = CGRectGetWidth(cropRect) - (newOriginX - cropRect.origin.x);
            cropRect.origin.x = newOriginX;
            cropRect.size = CGSizeMake(cappedWidth,
                                       !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
    }
    if (CGRectGetMaxY(rect) > CGRectGetMaxY(self.zoomingView.frame)) {
        if (angle == 0) {
            CGFloat cappedHeight =  CGRectGetMaxY([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinY(cropRect);
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
        else if (angle == 1) {
            float newOriginX = CGRectGetMinX([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            
            CGFloat cappedWidth = CGRectGetWidth(cropRect) - (newOriginX - cropRect.origin.x);
            cropRect.origin.x = newOriginX;
            cropRect.size = CGSizeMake(cappedWidth,
                                       !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
        else if (angle == -1) {
            CGFloat cappedWidth =  CGRectGetMaxX([self.scrollView convertRect:self.zoomingView.frame toView:self]) - CGRectGetMinX(cropRect);
            cropRect.size = CGSizeMake(cappedWidth, !self.keepingCropAspectRatio ? cropRect.size.height : cropRect.size.height * (cappedWidth/cropRect.size.width));
        }
        else if (angle == -3 || angle == 3) {
            float newOriginY = CGRectGetMinY([self.scrollView convertRect:self.zoomingView.frame toView:self]);
            CGFloat cappedHeight =  CGRectGetHeight(cropRect) - (newOriginY - cropRect.origin.y);
            cropRect.origin.y = newOriginY;
            cropRect.size = CGSizeMake(!self.keepingCropAspectRatio ? cropRect.size.width : cropRect.size.width * (cappedHeight / cropRect.size.height),
                                       cappedHeight);
        }
    }
    
    return cropRect;
}

- (void)automaticZoomIfEdgeTouched:(CGRect)cropRect
{
    //    if (CGRectGetMinX(cropRect) < CGRectGetMinX(self.editingRect) - 5.0f ||
    //        CGRectGetMaxX(cropRect) > CGRectGetMaxX(self.editingRect) + 5.0f ||
    //        CGRectGetMinY(cropRect) < CGRectGetMinY(self.editingRect) - 5.0f ||
    //        CGRectGetMaxY(cropRect) > CGRectGetMaxY(self.editingRect) + 5.0f) {
    //        [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
    //            [self zoomToCropRect:self.cropRectView.frame];
    //        } completion:NULL];
    //    }
}

#pragma mark -

- (void)cropRectViewDidBeginEditing:(PECropRectView *)cropRectView
{
    self.resizing = YES;
    needChangeAnchorPoint = YES;
}

- (void)cropRectViewEditingChanged:(PECropRectView *)cropRectView
{
    CGRect cropRect = [self cappedCropRectInImageRectWithCropRectView:cropRectView];
    
    [self layoutCropRectViewWithCropRect:cropRect];
    
    [self automaticZoomIfEdgeTouched:cropRect];
}

- (void)cropRectViewDidEndEditing:(PECropRectView *)cropRectView
{
    self.resizing = NO;
    [self zoomToCropRect:self.cropRectView.frame];
}

- (void)zoomToCropRect:(CGRect)toRect andCenter:(BOOL)center
{
    if (CGRectEqualToRect(self.scrollView.frame, toRect)) {
        return;
    }
    //    NSLog(@"befor1 cropRectView - x: %f y: %f w: %f h:%f",toRect.origin.x ,toRect.origin.y , toRect.size.width , toRect.size.height);
    CGFloat width = CGRectGetWidth(toRect);
    CGFloat height = CGRectGetHeight(toRect);
    
    CGFloat scale = MIN(CGRectGetWidth(self.editingRect) / width, CGRectGetHeight(self.editingRect) / height);
    
    CGFloat scaledWidth = width * scale;
    CGFloat scaledHeight = height * scale;
    CGRect cropRect = CGRectMake((CGRectGetWidth(self.bounds) - scaledWidth) / 2,
                                 (CGRectGetHeight(self.bounds) - scaledHeight) / 2,
                                 scaledWidth,
                                 scaledHeight);
    
    //    NSLog(@"after1 cropRectView - x: %f y: %f w: %f h:%f",cropRect.origin.x ,cropRect.origin.y , cropRect.size.width , cropRect.size.height);
    
    CGRect zoomRect = [self convertRect:toRect toView:self.zoomingView];
    float newScale = self.scrollView.zoomScale * scale;
    
    //    int angle = [self rotationAngle];
    
    //    if (angle == 0 || angle == -3 || angle == 3) {
    //        zoomRect.size.width = CGRectGetWidth(cropRect) / (self.scrollView.zoomScale * scale);
    //        zoomRect.size.height = CGRectGetHeight(cropRect) / (self.scrollView.zoomScale * scale);
    //    }
    //    else {
    //        zoomRect.size.height = CGRectGetWidth(cropRect) / (self.scrollView.zoomScale * scale);
    //        zoomRect.size.width = CGRectGetHeight(cropRect) / (self.scrollView.zoomScale * scale);
    //    }
    
    
    if(center) {
        CGRect imageViewBounds = self.imageView.bounds;
        zoomRect.origin.y = (CGRectGetHeight(imageViewBounds) / 2) - (CGRectGetHeight(zoomRect) / 2);
        zoomRect.origin.x = (CGRectGetWidth(imageViewBounds) / 2) - (CGRectGetWidth(zoomRect) / 2);
    }
//    CGRect bb1 = self.scrollView.bounds;
//    CGRect bb2 = [self.scrollView convertRect:self.scrollView.bounds toView:self.zoomingView];
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        
        self.scrollView.frame = cropRect;
        [self.scrollView zoomToRect:zoomRect animated:NO];
//        CGRect bb1 = self.scrollView.bounds;
        [self layoutCropRectViewWithCropRect:cropRect];
        [self resetMinimumZoomScale];
    } completion:NULL];
}

- (void)zoomToCropRect:(CGRect)toRect
{
    [self zoomToCropRect:toRect andCenter:NO];
}

#pragma mark -

- (void)handleRotation:(UIRotationGestureRecognizer *)gestureRecognizer
{
    CGFloat rotation = gestureRecognizer.rotation;
    
    CGAffineTransform transform = CGAffineTransformRotate(self.imageView.transform, rotation);
    self.imageView.transform = transform;
    gestureRecognizer.rotation = 0.0f;
    
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        self.cropRectView.showsGridMinor = YES;
    } else if (gestureRecognizer.state == UIGestureRecognizerStateEnded ||
               gestureRecognizer.state == UIGestureRecognizerStateCancelled ||
               gestureRecognizer.state == UIGestureRecognizerStateFailed) {
        self.cropRectView.showsGridMinor = NO;
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    return YES;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.zoomingView;
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset
{
    CGPoint contentOffset = scrollView.contentOffset;
    *targetContentOffset = contentOffset;
    needChangeAnchorPoint = YES;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    CGRect bounds  = self.scrollView.bounds;
    CGSize size = self.scrollView.contentSize;
    NSLog(@"\n scroll view bounds - W: %f H: %f \n scroll view size - w: %f h:%f \n zoomScale - %f",bounds.size.width , bounds.size.height , size.width , size.height, scrollView.zoomScale);
    
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(CGFloat)scale
{
    
}

- (UIImage *)image:(UIImage *)image rotatedByRadians:(CGFloat)radians
{
    // calculate the size of the rotated view's containing box for our drawing space
    UIView *rotatedViewBox = [[UIView alloc] initWithFrame:CGRectMake(0,0,image.size.width, image.size.height)];
    CGAffineTransform t = CGAffineTransformMakeRotation(radians);
    rotatedViewBox.transform = t;
    CGSize rotatedSize = rotatedViewBox.frame.size;
    
    // Create the bitmap context
    UIGraphicsBeginImageContext(rotatedSize);
    CGContextRef bitmap = UIGraphicsGetCurrentContext();
    
    // Move the origin to the middle of the image so we will rotate and scale around the center.
    CGContextTranslateCTM(bitmap, rotatedSize.width/2, rotatedSize.height/2);
    
    //   // Rotate the image context
    CGContextRotateCTM(bitmap, radians);
    
    // Now, draw the rotated/scaled image into the context
    CGContextScaleCTM(bitmap, 1.0, -1.0);
    CGContextDrawImage(bitmap, CGRectMake(-image.size.width / 2, -image.size.height / 2, image.size.width, image.size.height), [image CGImage]);
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
    
}

@end

